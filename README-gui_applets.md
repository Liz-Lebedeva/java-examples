## Sun Clock
GUI Java Apllet. Just for fun =)

![SunClock](images/sunclock600x450.gif)<p>

## Random Diagrams
GUI Java Apllet, which visualizes building graphs of random generator numbers.

#### A Bar Chart:

![RandomDiagram 1](images/diagram-1.gif)

#### A Line Chart:

![RandomDiagram 2](images/diagram-2.gif)

### Built With
- [Processing library](https://processing.org/)
