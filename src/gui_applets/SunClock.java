package gui_applets;
import processing.core.*;

public class SunClock extends PApplet {
    
    private String URL = "../data/wallpaper.jpg";
    private PImage backgroundImg;
    final int WIDTH = 800;
    final int HEIGHT = 600;
    
    public void setup() {
	size(WIDTH,HEIGHT);	
	backgroundImg = loadImage(URL);	
	noStroke();
    }
    
    public void draw() {
	showBackground();	
	showClockFace();	
	showSun();	
	showText();
    }
    
    private void showBackground() {
	backgroundImg.resize(width, height);
	image(backgroundImg, 0, 0);
    }
    
    private int setColor() {
	double color =  255 * (Math.abs(30 - second()) / 30.0);	
	return (int)(color);
    }
    
    private void showClockFace() {
	fill(setColor(), 80);
	int clockDiameter = min(width, height)*9/10;
	ellipse(width/2, height/2, clockDiameter, clockDiameter);
    }
    
    private void showSun() {
	// calc coordanates
	float angle = PI * 3/2 + radians(second() * 6);
	double radius = min(width, height) * 0.3;
	
	int x = (int)(width/2 + radius * cos(angle));
	int y = (int)(height/2 + radius * sin(angle));
	
	// draw ellipse
	fill(setColor(), setColor(), 0);	
	int sunDiameter = min(width, height)/6;
	ellipse(x, y, sunDiameter, sunDiameter);
    }

    private String setText(){
	String s = new String();
	int sec = second();
	
	if (sec == 59 || sec == 0 || sec == 1) {
	    s = "Noon";
	}
	else if (sec == 14 || sec == 15 || sec == 16) {
	    s = "Dusk";
	}
	else if (sec == 29 || sec == 30 || sec == 31) {
	    s = "Midnight";
	}
	else if (sec == 44 || sec == 45 || sec == 46) {
	    s = "Dawn";
	}	
	return s;
    }
    
    public void showText() {
	fill(255);
	textSize(height/10);
	textAlign(CENTER, CENTER);
	text(setText(), width/2, height/2);
    }
}
