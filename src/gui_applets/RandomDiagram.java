package gui_applets;
import processing.core.*;

public class RandomDiagram extends PApplet {

    float offset;
    float chartWidth;
    float chartHeight;
    float windowWidth;
    float windowHeight;
    int NumSegments;
    
    final static int BAROFFSET = 5;
    final static int MAXSEGMENTS = 100; // to do later
    final static int MAXFRAMERATE = 15; // to do later
    		
    float [] x = new float[MAXSEGMENTS + 2]; // to do later
    float [] y = new float[MAXSEGMENTS + 2]; 
    
    int i; 
    int switchCase;
    
    
    public void setup() {
	size(800,600);
	frameRate(2);
	i = 0;
	NumSegments = 21;
	switchCase = 0;	
    }
    
    public void draw() {	
	windowWidth = width;
	windowHeight = height;
	offset = min(windowWidth, windowHeight) / 10;
	chartWidth = windowWidth - offset * 2;
	chartHeight = windowHeight - offset * 2;
	
	setGradient(148, 148, 184, 51, 51, 77);
	
	if (switchCase == 0) {
	    showBase();
	    showText(windowWidth/2 , offset + chartHeight/5 , "for the Bar Chart please press '1'", chartHeight/20);
	    showText(windowWidth/2 , offset + chartHeight * 2/5 , "for the Line Chart please press '2'", chartHeight/20);
	} 
	else if (switchCase == 1) {
	    showBase();
	    showBarChart();
	}	
	else if (switchCase == 2) {
	    showBase();
	    showLineChart();
	}
    }
    
    public void keyPressed() {
	if (key == TAB) {
	    i = 0;
	    switchCase = 0;
	    y = new float[MAXSEGMENTS + 2];
	}
	if (key == '1') {
	    switchCase = 1;
	}
	if (key == '2') {
	    switchCase = 2;
	}
	if (key == ' ') {
	    noLoop();
	}
    }
    
    public void keyReleased() {
	if (key == ' ') {
	    loop();
	}
    }
    
    // method draws background gradient
    private void setGradient(int r1, int g1, int b1, int r2, int g2, int b2) {
	//color c = color(255, 204, 0);
	
	//noFill();
	for (int i = 1; i < windowHeight; i++) {
	    float inter = i / windowHeight;
	    stroke(lerp(r1, r2, inter), lerp(g1, g2, inter), lerp(b1, b2, inter));
	    line(0, i, windowWidth, i);
	}
    }
    
    // method draws transparent Chart Area
    private void showBase() {
	fill(255, 80);
	noStroke();
	rect(offset - BAROFFSET, offset - BAROFFSET, chartWidth + 2*BAROFFSET, chartHeight + 2*BAROFFSET);
    }

    // method draws Line Chart
    private void showLineChart() {
	showText(windowWidth/2 , offset * 1/6 , "to pause execution please press 'Space'", offset/4);
	showText(windowWidth/2 , offset * 1/2 , "to return back to the Menu please press 'Tab'", offset/4);
		
	if (i < NumSegments) {	    
	    y[i] = calcY(i);
	    drawLines(i);
	    i++;	    
	} 
	else {
	    drawLines(NumSegments);
	}
    }

    private void drawLines (int iteration) {
	fill(255);
	showText(offset/2, offset, chartHeight, offset/5);
	showText(offset/2, windowHeight - offset, 0, offset/5);
		
	float total = 0;
	for (int x = 0; x < min(iteration, y.length); x++) {
	    if (x != 0) {
		drawOneLine(x);		
	    }
	    drawOneMarker(x);
	    total += chartHeight * y[x];
	}
	if (total != 0) {
	    drawAverage(total / (iteration + 1));
	}
    }
    
    // method draws an individual line for the Line Chart
    private void drawOneLine(int counterX) {
	float step = chartWidth/NumSegments;
	float y1 = chartHeight * y[counterX - 1];
	float y2 = chartHeight * y[counterX];
	
	float pointX1 = offset + (counterX - 1) * step + step/2;
	float pointX2 = offset + counterX * step + step/2;
	float pointY1 = windowHeight - offset - y1;
	float pointY2 = windowHeight - offset - y2;
		
	stroke(45, 45, 134);
	//stroke(255);
	line(pointX1, pointY1, pointX2, pointY2);
    }
    
    // method draws individual marker for the Line Chart
    private void drawOneMarker(int counterX) {
	float step = chartWidth/NumSegments;
	float markerHeight = chartHeight * y[counterX];
	
	float centerX = offset + counterX * step + step/2;
	float centerY = windowHeight - offset - markerHeight;
	
	fill(255, 140, 26, 50);
	stroke(255, 140, 26);
	ellipse(centerX, centerY, offset/6 + 2, offset/6 + 2);
	ellipse(centerX, centerY, offset/6 + 2, offset/6 + 2);
	
	float [] textOffset = setTextOffset(counterX);
	fill(45, 45, 134);
	//fill(255);
	showText(centerX + textOffset[0], centerY - textOffset[1], markerHeight, offset/5);
	
    }
    
    // method draws Bar Chart
    private void showBarChart() {
	showText(windowWidth/2 , offset * 1/6 , "to pause execution please press 'Space'", offset/4);
	showText(windowWidth/2 , offset * 1/2 , "to return back to the Menu please press 'Tab'", offset/4);
		
	if (i < NumSegments) {	    
	    y[i] = calcY(i);
	    drawBars(i);	    
	    i++;
	} 
	else {
	    drawBars(NumSegments);
	}
    }
    
    // method draws a specified (counter) number of bars for the Bar Chart (used in the animation loop)
    private void drawBars (int iteration) {
	fill(255);
	showText(offset/2, offset, chartHeight, offset/5);
	showText(offset/2, windowHeight - offset, 0, offset/5);
	
	float total = 0;
	for (int x = 0; x < min(iteration, y.length); x++) {
	    drawOneBar(x);
	    total += chartHeight * y[x];
	}
	if (total != 0) {
	    drawAverage(total / (iteration + 1));
	}
    }
    
    // method draws an individual bar for the Bar Chart with a text
    private void drawOneBar(int counterX) {
	float barWidth = chartWidth/NumSegments;
	float barHeight = chartHeight * y[counterX];
	
	float pointX1 = offset + counterX * barWidth;
	float pointY1 = windowHeight - offset - barHeight;	
	
	fill(255, 140, 26, 180);
	stroke(255, 140, 26);
	rect(pointX1 + BAROFFSET, pointY1, barWidth - BAROFFSET * 2, barHeight);
	
	fill(255);
	showText((pointX1 + barWidth/2), (windowHeight - offset * 3/4), barHeight, offset/5);
    }
    
    // method draws a horizontal line and shows text with its Y-coordinate
    private void drawAverage(float average) {
	float lineY = windowHeight - offset - average;
	stroke(255);
	//stroke(45, 45, 134);
	line(offset, lineY, offset + chartWidth - 1, lineY);
	
	fill(255);
	//fill(45, 45, 134);
	showText(offset/2, lineY, average, offset/5);
    }

    // method shows specified text in specified location
    private void showText(float x, float y, String txt, float size) {
	//noStroke();
	fill(255);
	textSize(size);
	textAlign(CENTER, CENTER);
	text(txt, x, y);
    }

    // method shows specified number in specified location
    private void showText(float x, float y, float number, float size) {
	//noStroke();
	//fill(255);
	textSize(size);
	textAlign(CENTER, CENTER);
	text(Integer.toString((int)(number)), x, y);
    }
    
    // method to position the text relative to its marker
    private float[] setTextOffset(int counterX) {
	float [] textOffsetXY = new float[2];
	float defaultOffset = offset/4;
	
	if (counterX == 0) {
	    textOffsetXY[0] = 0;
	    
	    if (y[counterX + 1] > y[counterX]){    
		textOffsetXY[1] = - defaultOffset;	    
	    } 
	    else if (y[counterX + 1] <= y[counterX]) {
		textOffsetXY[1] = defaultOffset * 4/3;
	    }
	    
	    if (y[counterX] < 0.05) {
		textOffsetXY[1] = defaultOffset * 3/2;
	    }
	    if (y[counterX] > 0.95) {
		textOffsetXY[1] = - defaultOffset * 3/2;
	    } 
	} 
	else if (counterX == (NumSegments - 1)) {
	    textOffsetXY[0] = 0;
	    
	    if (y[counterX - 1] > y[counterX]){    
		textOffsetXY[1] = - defaultOffset;	    
	    } 
	    else if (y[counterX - 1] <= y[counterX]) {
		textOffsetXY[1] = defaultOffset * 4/3;
	    }
	    
	    if (y[counterX] < 0.05) {
		textOffsetXY[1] = defaultOffset * 3/2;
	    }
	    if (y[counterX] > 0.95) {
		textOffsetXY[1] = - defaultOffset * 3/2;
	    } 
	    
	} 
	else {
	    // low
	    if (y[counterX - 1] > y[counterX] && y[counterX + 1] > y[counterX]) {
		if (y[counterX] < 0.05) {
		    textOffsetXY[1] = 0;
		    if (y[counterX - 1] <= y[counterX + 1]) {
			textOffsetXY[0] = defaultOffset * 3/2;
		    }
		    else {
			textOffsetXY[0] = -defaultOffset * 3/2;
		    }
		}
		else {
		    textOffsetXY[0] = 0;
		    textOffsetXY[1] = -defaultOffset;
		}
	    } 
	    // up
	    else if (y[counterX - 1] <= y[counterX] && y[counterX + 1] > y[counterX]) {
		textOffsetXY[0] = defaultOffset;
		textOffsetXY[1] = - defaultOffset;
	    } 
	    // down
	    else if (y[counterX - 1] > y[counterX] && y[counterX + 1] <= y[counterX]) {
		textOffsetXY[0] = - defaultOffset;
		textOffsetXY[1] = - defaultOffset;
	    } 
	    // peak
	    else if (y[counterX - 1] <= y[counterX] && y[counterX + 1] <= y[counterX]) {
		if (y[counterX] > 0.95) {
		    textOffsetXY[1] = 0;
		    if (y[counterX - 1] >= y[counterX + 1]) {
			textOffsetXY[0] = defaultOffset * 3/2;
		    }
		    else {
			textOffsetXY[0] = -defaultOffset * 3/2;
		    }
		}
		else {
		    textOffsetXY[0] = 0;
		    textOffsetXY[1] = defaultOffset * 4/3;
		}
	    }
	}
	return textOffsetXY;
    }
    
    // method to calculate variable X (used in 2D Point Chart random generated) - to do later
    private float calcX () {
	return random(1);
    }

    // method to calculate variable Y, which depends on X (here Y is random generated)
    private float calcY (int x) {
	return random(1);
    }    
}

