# java-examples
This repository is my Java playground (projects, created to practice a new staff I'm learning from the [Java course on Coursera](https://www.coursera.org/specializations/java-object-oriented))

### Including:
- GUI Applets ([see description](https://gitlab.com/Liz-Lebedeva/java-examples/blob/master/README-gui_applets.md))
